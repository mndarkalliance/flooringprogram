﻿using System;
using System.Configuration;
using FlooringProgram.Data;
using FlooringProgram.Operations;
using NUnit.Framework;

namespace FlooringProgram.OperationsTests
{
    [TestFixture]
    public class OrderOperationsTests
    {
        bool _functionsProperly;

        //
        [Test]
        public void RemoveOrderTest()
        {
            string date = DateTime.Now.ToString("MMddyyyy"); 

            ConfigurationManager.AppSettings["mode"] = "Test";

            OrderTestRepository newRepository = new OrderTestRepository();

            int fileLengthBefore = newRepository.NextOrderNumber(date);

            _functionsProperly = OrderOperations.RemoveOrder(date, 3);

            int fileLengthAfter = newRepository.NextOrderNumber(date);

            Assert.AreNotEqual(fileLengthBefore, fileLengthAfter);

        }
    }
}