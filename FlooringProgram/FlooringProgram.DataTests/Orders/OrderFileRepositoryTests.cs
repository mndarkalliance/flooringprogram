﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using FlooringProgram.Data;
using FlooringProgram.Models;
using FlooringProgram.Operations;
using NUnit.Framework;

namespace FlooringProgram.DataTests.Orders
{

    [TestFixture]
    public class OrderFileRepositoryTests
    {

        [Test]
        public void SaveTest()
        {
            ConfigurationManager.AppSettings["mode"] = "Test";
            OrderTestRepository testFileRepository = new OrderTestRepository();

            int beforeSave = OrderOperations.GetNextOrderNumber(
                DateTime.Now.ToString("MMddyyyy")); // 3 orders initially

            OrderTestRepository.TestOrders.Add(new Order()); // add new order
            
            int afterSave = OrderOperations.GetNextOrderNumber(
                DateTime.Now.ToString("MMddyyyy")); // 4 orders after

            Assert.AreNotEqual(beforeSave, afterSave);
        }

        [Test]
        public void GetOrders()
        {
            ConfigurationManager.AppSettings["mode"] = "Test";
            OrderTestRepository testFileRepository = new OrderTestRepository();

            List<Order> sampleOrders = testFileRepository.GetOrders(
                DateTime.Now.ToString("MMddyyyy")); // gets sample orders

            var numOrders = sampleOrders.Count(); // should be greater than 0

            Assert.AreNotEqual(0, numOrders);
        }
    }
}