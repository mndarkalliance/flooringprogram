﻿using System;
using System.Configuration;

namespace FlooringProgram.UI
{
    internal class Menu
    {
        public static string Input;
        private static string _currentMode;
        private static string _currentFormat;

        private static string DisplayMode()
        {
            if (ConfigurationManager.AppSettings["mode"] == "Prod")
            {
                return "production";
            }

            return "test";
        }

        private static string DisplayFormat()
        {
            if (ConfigurationManager.AppSettings["FileType"] == "csv")
            {
                return "CSV";
            }
            return "JSON";
        }

        public static string MenuText =
            @"
   *******************************************************************************
   *    /$$$$$$$$/$$                           /$$                               *
   *   | $$_____| $$                          |__/                               *
   *   | $$     | $$ /$$$$$$  /$$$$$$  /$$$$$$ /$$/$$$$$$$  /$$$$$$              *
   *   | $$$$$  | $$/$$__  $$/$$__  $$/$$__  $| $| $$__  $$/$$__  $$             *
   *   | $$__/  | $| $$  \ $| $$  \ $| $$  \__| $| $$  \ $| $$  \ $$             *
   *   | $$     | $| $$  | $| $$  | $| $$     | $| $$  | $| $$  | $$             *
   *   | $$     | $|  $$$$$$|  $$$$$$| $$     | $| $$  | $|  $$$$$$$             *
   *   |__/     |__/\______/ \______/|__/     |__|__/  |__/\____  $$             *
   *                                                       /$$  \ $$             *
   *               PROGRAM                                 |  $$$$$$/            *
   *                                                        \______/             *
   *                                                                             *
   *   1. Display Orders                                                         *
   *   2. Add an Order                                                           *
   *   3. Edit an Order                                                          *
   *   4. Remove an Order                                                        *
   *   5. Change Program Mode                                                    *
   *   6. Change File Mode                                                       *
   *   7. Quit                                                                   *
   *                                                                             *
   *******************************************************************************";

        // Display menu screen and report modes. Program mode = test/prod; File mode = csv/json
        public static void DisplayMenu()
        {
            _currentMode = DisplayMode();
            _currentFormat = DisplayFormat();
            Console.WriteLine(MenuText);
            Console.WriteLine(
                $"    **You are in {_currentMode} mode and your orders are saving in the {_currentFormat} format.** \n");
        }




        // prompt user to choose menu option and call appropriate function
        public static void MenuPrompt()
        {

            while (true) // return to menu after every function completes
            {            
                Console.Clear();
                DisplayMenu();

                Console.Write("\n\n\t\tEnter an option: ");

                Input = Console.ReadLine();
                Input = Input.Replace(" ", "");

                try
                {
                    switch (Input) // Ensure that input is 1-5 and call appropriate function
                    {
                        case "1":
                            Program.DisplayOrder();
                            break;
                        case "2":
                            Program.AddOrder();
                            break;
                        case "3":
                            Program.EditOrder();
                            break;
                        case "4":
                            Program.RemoveOrder();
                            break;
                        case "5":
                            Program.ModePrompt();
                            break;
                        case "6":
                            Program.FileFormatPrompt();
                            break;
                        case "7":
                            return;
                        default:
                            Console.SetCursorPosition(0, Console.CursorTop - 3);
                            Prompt.ErrorWriter("\t\tInvalid entry. Press Any key to Try again.");
                            Console.ReadKey();
                            break;
                    }
                }
                catch
                {
                    Prompt.ErrorWriter(
                        "\n\t\tAn error occurred and has been logged.\n\t\tPress any key to return to menu.");
                    Console.ReadKey();
                }
            }
        }
    }
}

