﻿using System;
using System.Threading;
using FlooringProgram.Models;
using FlooringProgram.Operations;

namespace FlooringProgram.UI
{
    class Prompt
    {
        static bool _isValidDate;
        static bool _isValidOrderNumber;


        #region Prompts for AddOrder
        // prompts user to enter their state
        public static TaxRate StateTaxPrompt()
        {
            AddPrompter("State");

            while (true)
            {
                var input = Console.ReadLine();
                if (input != null)
                {
                    string state = input.Replace(" ", "").ToUpper();

                    TaxRateOperations taxOps = new TaxRateOperations();

                    if (taxOps.IsAllowedState(state))
                    {
                        Console.Clear();
                        return taxOps.GetTaxRateFor(state);
                    }
                }

                AddPrompterWithError("State");
            }
        }

        // gets area from the user
        public static double AreaPrompt()
        {
            AddPrompter("Area");

            while (true)
            {
                string areaStr = Console.ReadLine();
                double area;

                if (!string.IsNullOrEmpty(areaStr) && double.TryParse(areaStr, out area))
                {
                    Console.Clear();
                    return area;
                }

                AddPrompterWithError("Area");
            }
        }

        // asks the user for their name
        public static string CustomerPrompt()
        {
            AddPrompter("Customer Name");

            while (true)
            {
                string name = Console.ReadLine();

                if (!(name.Replace(" ", "") == ""))
                {
                    Console.Clear();
                    return name;
                }
                AddPrompterWithError("Customer Name");

            }
        }

        //makes user choose between the four products and returns that product
        public static Product ProductPrompt()
        {
            Product customerProduct;

            AddPrompter("Product Name");

            while (true)
            {
                string productName = Console.ReadLine();

                if (!string.IsNullOrEmpty(productName))
                {
                    customerProduct = ProductOperations.GetProduct(productName);

                    if (customerProduct != null)
                    {
                        Console.Clear();
                        return customerProduct;
                    }
                }

                AddPrompterWithError("Product Name");
            }
        }


        private static void AddPrompter(string fieldName)
        {
            Console.WriteLine("\n\t\t\tEnter Order Details");
            Console.Write($"\n\n\n\t\t{fieldName}: ");
        }

        private static void AddPrompterWithError(string fieldName)
        {
            Console.Clear();
            Console.WriteLine("\n\t\t\tEnter Order Details");
            ErrorWriter($"\n\t\tInvalid {fieldName.ToLower()}. Please try again.");
            Console.Write($"\n\n\t\t{fieldName}: ");
        } 
        #endregion



        //takes user input and returns true if the user wants to save their order, and false if not
        public static bool SaveFilePrompt()
        {
            Console.Write("\n\n\n\t\tWould You like to save your order? (Y/N): ");
            
            while (true)
            {
                string input = Console.ReadLine().Replace(" ", "");

                if (input.ToUpper() == "Y")
                {
                    return true;
                }
                if (input.ToUpper() == "N")
                {

                    return false;
                }
                

                DeleteLine(3);
                Console.SetCursorPosition(0, Console.CursorTop - 4);
                ErrorWriter("\n\t\tInvalid entry. Please try again.\n\n");

                DeleteLine(0);

                Console.Write("\t\tWould You like to save your order? (Y/N): ");
                
            }

        }

        //the prompt when the user wants to delete an order
        public static bool DeleteOrderPrompt()
        {
            Console.Write("\n\n\n\t\tThis is the order.  Are you sure you want to delete it? (Y/N): ");

            while (true)
            {
                string input = Console.ReadLine().Replace(" ", "");

                if (input.ToUpper() == "Y")
                {
                    return true;
                }
                if (input.ToUpper() == "N")
                {

                    return false;
                }

                DeleteLine(3);
                Console.SetCursorPosition(0, Console.CursorTop - 4);
                ErrorWriter("\n\t\tInvalid entry. Please try again.\n\n");
                DeleteLine(0);

                Console.Write("\t\tWould You like to delete your order? (Y/N): ");

            }

        }


        //gets and validates the date and order number from the user. parameters must be declared before
        //before the method is called, since the method also alters those values to proper formating
        public static void PromptOrderInfo(out string orderDate, out int orderNumber)
        {
            do
            {
                Console.Write("\n\t\tEnter a date in the format of (ddmmyyy): ");
                orderDate = Console.ReadLine();

                _isValidDate = Validation.ValidateDate(ref orderDate);
                if (_isValidDate)
                {
                    continue;
                }
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\tInvalid Date. Please try again.\n");
                Console.ResetColor();

            } while (!_isValidDate);

            do
            {
                Console.Write("\n\t\tEnter an order number: ");
                string orderNumberStr = Console.ReadLine();
                orderNumberStr = orderNumberStr.Replace(" ", "");

                _isValidOrderNumber = int.TryParse(orderNumberStr, out orderNumber);
                if (_isValidOrderNumber)
                {
                    continue;
                }
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\tInvalid order number. Please try again.");
                Console.ResetColor();

            } while (!_isValidOrderNumber);
        }
      
         
        //takes in a string and scrolls it across screen in red
        public static void ErrorWriter(string str)
        {
            foreach (var c in str)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(c);
                Thread.Sleep(10);
                Console.ResetColor();
            }
        }

        //deletes a certain amount of lines so the input area can be 
        //cleared without clearing the rest of the screen
        public static void DeleteLine(params int[] linesAbove)
        {
            foreach (int n in linesAbove)
            {
                int returnLineCursor = Console.CursorTop;

                Console.SetCursorPosition(0, Console.CursorTop - n);
                Console.WriteLine(new string(' ', Console.WindowWidth));

                Console.SetCursorPosition(0, returnLineCursor);
            }

        }
       
    }
}
