﻿namespace FlooringProgram.UI
{
    class Validation
    {
        // ensures that entered date is valid (ie any 8 digit int)
        public static bool ValidateDate(ref string input)
        {
            bool isValidDate = false;

            input = input.Replace(" ", "");

            if (input.Length == 8)
            {
                int date;
                isValidDate = int.TryParse(input, out date);
            }

            return isValidDate;
       
        }
    }
}
