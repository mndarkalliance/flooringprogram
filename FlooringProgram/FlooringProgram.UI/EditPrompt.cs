﻿using System;
using System.Windows.Forms;
using FlooringProgram.Models;
using FlooringProgram.Operations;

namespace FlooringProgram.UI
{
    // contains order prompts from Prompt class, but calls sendkeys on the original property data (enabling user editing)
    public class EditPrompt
    {
        public static TaxRate StateTaxPrompt(Order order)
        {
            EditPrompter(order.StateTaxRate.State, "State");

            while (true)
            {
                string state = Console.ReadLine().Replace(" ", "");

                TaxRateOperations taxOps = new TaxRateOperations();

                if (taxOps.IsAllowedState(state))
                {
                    Console.Clear();
                    return taxOps.GetTaxRateFor(state);

                }

                EditPrompterWithError(order.StateTaxRate.State, "State");
            }
        }


        public static double AreaPrompt(Order order)
        {
            EditPrompter(order.Area.ToString(), "Area");

            while (true)
            {
                string areaStr = Console.ReadLine();
                double area;

                if (!string.IsNullOrEmpty(areaStr) && double.TryParse(areaStr, out area))
                {
                    Console.Clear();
                    return area;
                }

                EditPrompterWithError(order.CustomerName, "Customer Name");
            }
        }

        public static string CustomerPrompt(Order order)
        {
            EditPrompter(order.CustomerName, "Customer Name");

            while (true)
            {
                string name = Console.ReadLine();

                if (!(name.Replace(" ", "") == ""))
                {
                    Console.Clear();
                    return name;
                }

                EditPrompterWithError(order.CustomerName, "Customer Name");
            }
        }


        public static Product ProductPrompt(Order order)
        {
            Product customerProduct = new Product();

            EditPrompter(order.ProductInfo.ProductType, "Product Name");

            while (true)
            {
                string productName = Console.ReadLine();

                if (!string.IsNullOrEmpty(productName))
                {
                    customerProduct = ProductOperations.GetProduct(productName);

                    if (customerProduct != null)
                    {
                        Console.Clear();
                        return customerProduct;
                    }

                }

                EditPrompterWithError(order.ProductInfo.ProductType, "Product Name");
            }
        }

        private static void EditPrompter(String field, string fieldName)
        {
            Console.WriteLine("\n\t\t\tEnter Order Details");
            Console.Write($"\n\n\n\t\t{fieldName}: ");
            SendKeys.SendWait(field);
        }

        private static void EditPrompterWithError(string orderField, string fieldName)
        {
            Console.Clear();
            Console.WriteLine("\n\t\t\tEnter Order Details");
            Prompt.ErrorWriter("\n\t\tInvalid entry. Please try again.");
            Console.Write($"\n\n\t\t{fieldName}: ");
            SendKeys.SendWait(orderField);
        }
    }
}
