﻿using System;
using System.Configuration;
using FlooringProgram.Models;
using FlooringProgram.Operations;

namespace FlooringProgram.UI
{
    
    class Program
    {

        private readonly static string TodaysDate = DateTime.Now.ToString("MMddyyyy");

        static void Main(string[] args)
        {
            // Adjust console size to improve UI exp
            Console.WindowWidth = 92;
            Console.WindowHeight = 43;

            Menu.MenuPrompt();  
        }

       
        // prompt user for order info and remove specified order
        public static void RemoveOrder()
        {
            
            string orderDate;
            int orderNumber;

            Console.WriteLine("\n\t\tLet's find the order that you want to delete.");

            
            while (true)
            {
                Prompt.PromptOrderInfo(out orderDate, out orderNumber);
                if ((OrderOperations.CheckForFile(orderDate)) && (OrderOperations.GetNextOrderNumber(orderDate) > orderNumber))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("\n\t\t\tI'm sorry but, I couldn't find your order.\n\t\t\tPlease check your records to try again and press any key to continue.");

                }

            }

            Console.Clear();

            // get order to be removed
            Order orderToDisplay = OrderOperations.GetOrder(orderDate, orderNumber); 

            if (orderToDisplay != null) // get display order to be removed
            {
                WriteOrder(orderToDisplay);
            }

            if (Prompt.DeleteOrderPrompt()) // remove order if user confirms removal
            {
                if (OrderOperations.RemoveOrder(orderDate, orderNumber))
                {
                    Console.WriteLine("\n\t\t\tIt worked! Order removed. Press any key to continue.");

                }
                else
                {
                    Console.WriteLine("\n\t\t\t\tYour deletion request failed.  Please check your records and try again.\n\t\t\tPress any key to try again.");
                }
            }
            else
            {
                Console.WriteLine("\n\t\t\t\tOk! I'll hold onto it for now.");
            }

            Console.WriteLine("\n\t\t\t\tPress any key to continue.");
            Console.ReadKey();
     

        }
        
        // prompt user for order info and add order to appropriate repo
        public static void AddOrder()
        {
            Console.Clear();

            var orderToAdd = new Order();

            CreateOrder(orderToAdd);

            Console.WriteLine("\n\t\t\tORDER SUMMARY\n");
            WriteOrder(orderToAdd);

            if (Prompt.SaveFilePrompt())
            {
                if (OrderOperations.SaveOrder(orderToAdd, TodaysDate))
                {
                    Console.WriteLine("\n\n\t\t\tFile was saved.");
                }
                else
                {
                    Console.WriteLine("\n\n\t\t\tError: File was not saved.");
                }
            }

            Console.WriteLine("\n\t\t\tPress any key to continue.");
            Console.ReadKey();
   


        }

        // prompt user for identifying order information and let user edit order
        public static void EditOrder()
        {
            string orderDate;
            int orderNumber;

            Prompt.PromptOrderInfo(out orderDate, out orderNumber);

            // Get order to remove
            Order orderToEdit = OrderOperations.GetOrder(orderDate, orderNumber);

            if (orderToEdit != null)
            {
                Console.Clear();
                EditOrder(orderToEdit); 

                Console.Clear();
                WriteOrder(orderToEdit);

                if (OrderOperations.SaveEditedOrder(orderToEdit, orderDate))
                {
                    Console.WriteLine("\n\n\t\t\tYour edits have been saved");
                }
                else
                {
                    Console.WriteLine("\n\n\t\t\tError: Your edits could not be saved.");
                };
                
            }
            else
            {
                Prompt.ErrorWriter("\n\n\n\t\tThe order does not exist.");

            }

            Console.WriteLine("\n\t\t\tPress any key to continue.");
            Console.ReadKey();

        }
        
        // prompts user to input each field of a new order
        private static void CreateOrder(Order orderToAdd)
        {
            orderToAdd.CustomerName = Prompt.CustomerPrompt();
            orderToAdd.StateTaxRate = Prompt.StateTaxPrompt();
            orderToAdd.ProductInfo = Prompt.ProductPrompt();
            orderToAdd.Area = Prompt.AreaPrompt();

            orderToAdd.LaborCost = TotalsCalc.LaborCostCalculator(orderToAdd);
            orderToAdd.MaterialCost = TotalsCalc.MaterialCostCalculator(orderToAdd);

            orderToAdd.Tax = TotalsCalc.TaxCalculator(orderToAdd);
            orderToAdd.Total = TotalsCalc.TotalCalculatorCalculator(orderToAdd);

            // if file doesn't exist, order number is 1, else if file does exist,
            // make order number equal line count in file. 
            orderToAdd.OrderNumber = !OrderOperations.CheckForFile(TodaysDate) ? 1 : OrderOperations.GetNextOrderNumber(TodaysDate);
            

        }

        public static void EditOrder(Order ordertoEdit)
        {
            ordertoEdit.CustomerName = EditPrompt.CustomerPrompt(ordertoEdit);
            ordertoEdit.StateTaxRate = EditPrompt.StateTaxPrompt(ordertoEdit);
            ordertoEdit.ProductInfo = EditPrompt.ProductPrompt(ordertoEdit);
            ordertoEdit.Area = EditPrompt.AreaPrompt(ordertoEdit);

            ordertoEdit.LaborCost = TotalsCalc.LaborCostCalculator(ordertoEdit);
            ordertoEdit.MaterialCost = TotalsCalc.MaterialCostCalculator(ordertoEdit);

            ordertoEdit.Tax = TotalsCalc.TaxCalculator(ordertoEdit);
            ordertoEdit.Total = TotalsCalc.TotalCalculatorCalculator(ordertoEdit);
            
        }

        //this method displays the order; WriteOrder writes the order to the console, 
        //using the BLL method GetOrder 
        public static void DisplayOrder()
        {
            string orderDate;
            int orderNumber; 

            Prompt.PromptOrderInfo(out orderDate, out orderNumber);

            Order orderToDisplay = OrderOperations.GetOrder(orderDate, orderNumber);

            Console.Clear();
            if (orderToDisplay != null)
            {
                WriteOrder(orderToDisplay);
            }
            else
            {
                Prompt.ErrorWriter("\n\n\n\t\tThe order does not exist.");    
            }

            Console.WriteLine("\n\n\t\tPress Any Key to Continue.");
            Console.ReadKey();
          
        }

        //this takes an Order and writes its fields to the console
        private static void WriteOrder(Order orderToDisplay)
        {
            Console.WriteLine($"\n\tOrder Number: {orderToDisplay.OrderNumber}");
            Console.WriteLine($"\tCustomer Name: {orderToDisplay.CustomerName}");
            Console.WriteLine($"\tState: {orderToDisplay.StateTaxRate.State}");
            Console.WriteLine($"\tTax Rate: {orderToDisplay.StateTaxRate.TaxPercent}");
            Console.WriteLine($"\tArea: {orderToDisplay.Area}");
            Console.WriteLine($"\tProduct Type: {orderToDisplay.ProductInfo.ProductType}");
            Console.WriteLine($"\tLabor Cost Per Square Foot: {orderToDisplay.ProductInfo.LaborCostPerSquareFoot}");
            Console.WriteLine($"\tMaterial Cost Per Square Foot: {orderToDisplay.ProductInfo.MaterialCostPerSquareFoot}");
            Console.WriteLine($"\tMaterial Cost: {orderToDisplay.MaterialCost}");
            Console.WriteLine($"\tLabor Cost: {orderToDisplay.LaborCost}");
            Console.WriteLine($"\tTax: {orderToDisplay.Tax}");
            Console.WriteLine($"\tTotal: {orderToDisplay.Total}");

        }

        //this method lets the user decide which mode: Prod or Test
        public static void ModePrompt()
        {
            Prompt.DeleteLine(1);
            bool isValidMode = false;
            Console.SetCursorPosition(0, Console.CursorTop - 2);
            do
            {
                Console.Write("\n\t    Which mode you would like to use? (Production or Test): ");

                string userMode = Console.ReadLine();
                Prompt.DeleteLine(1);
                if (userMode.ToLower() == "production")
                {
                    Console.SetCursorPosition(0, Console.CursorTop - 2);
                    ConfigurationManager.AppSettings["mode"] = "Prod";
                    Console.WriteLine("\n\t\t\t   You are now in production mode.");
                    isValidMode = true;
                }
                else if (userMode.ToLower() == "test")
                {
                    Console.SetCursorPosition(0, Console.CursorTop - 2);
                    ConfigurationManager.AppSettings["mode"] = "Test";
                    Console.WriteLine("\n\t\t\t     You are now in test mode.");
                    isValidMode = true;
                }
                else
                {
                    Prompt.DeleteLine(5);
                    Console.SetCursorPosition(0, Console.CursorTop - 6);
                    Prompt.ErrorWriter("\n\t\t\tInvalid entry. Please try again.\n\n\n");
                }

            } while (!isValidMode);

            Console.WriteLine("\n\t\t\t     Press any key to continue.");
            Console.ReadKey();
            
        }

        public static void FileFormatPrompt()
        {
            Prompt.DeleteLine(1);
            bool isValidFormat = false;
          //  Console.SetCursorPosition(0, Console.CursorTop - 2);

            do
            {
                Console.Write("\n\t\tWhich file format would you like to use? (CSV or JSON): ");

                string fileFormat = Console.ReadLine();
                Prompt.DeleteLine(1);
                if (fileFormat.ToUpper() == "CSV")
                {
                    Console.SetCursorPosition(0, Console.CursorTop - 2);
                    ConfigurationManager.AppSettings["FileType"] = "csv";
                    Console.WriteLine("\t\t   Your files will save in the CSV format.");
                    isValidFormat = true;
                }
                else if (fileFormat.ToUpper() == "JSON")
                {
                    Console.SetCursorPosition(0, Console.CursorTop - 2);
                    ConfigurationManager.AppSettings["FileType"] = "json";
                    Console.WriteLine("\t\t   Your files will save in the JSON format.");
                    isValidFormat = true;
                }
                else
                {
                    Prompt.DeleteLine(5);
                    Console.SetCursorPosition(0, Console.CursorTop - 6);
                    Prompt.ErrorWriter("\n\t\t\tInvalid entry. Please try again.\n\n\n");
                }

            } while (!isValidFormat);

            Console.WriteLine("\n\t\t\t Press any key to continue.");
            Console.ReadKey();
            
        }
    }
}
