﻿using System.Collections.Generic;

namespace FlooringProgram.Models.Interfaces
{
    public interface IOrderRepository
    {
        List<Order> GetOrders(string date);

        bool Save(List<Order> orderList, string date);

        bool CheckForFile(string date);

        Order OrderReader(string date, int orderNumber);

        int NextOrderNumber(string date);

        bool DeleteFile(string date);
    }
}
