﻿namespace FlooringProgram.Models.Interfaces
{
    public interface IProductRepository
    {
        Product ProductReader(string productType);
    }
}
