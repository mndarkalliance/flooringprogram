﻿using System.Configuration;

namespace FlooringProgram.Data
{
    public class ConfigurationSettings
    {
        private static string _mode;

        public static string GetMode()
        {

            _mode = ConfigurationManager.AppSettings["mode"];

            return _mode;
        }

    }
}
