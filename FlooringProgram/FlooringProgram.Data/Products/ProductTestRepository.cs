﻿using System.Collections.Generic;
using System.Linq;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.Products
{

    public class ProductTestRepository : IProductRepository
    {

        public static List<Product> TestProducts = new List<Product>()
        {
            new Product()
            {
                ProductType = "Carpet",
                MaterialCostPerSquareFoot = 2.25M,
                LaborCostPerSquareFoot = 2.10M,
            },
            new Product()
            {
                ProductType = "Laminate",
                MaterialCostPerSquareFoot = 1.75M,
                LaborCostPerSquareFoot = 2.10M,
            },
            new Product()
            {
                ProductType = "Tile",
                MaterialCostPerSquareFoot = 3.50M,
                LaborCostPerSquareFoot = 4.15M,
            },
            new Product()
            {
                ProductType = "Wood",
                MaterialCostPerSquareFoot = 5.15M,
                LaborCostPerSquareFoot = 4.75M,
            }
        };

        public Product ProductReader(string productType)
        {
            var products = TestProducts.Where(p => p.ProductType.ToLower() == productType.ToLower());
            
            if (products.Any())
            {
                return products.First();

            }
            return null;
            
        }
    }
}
