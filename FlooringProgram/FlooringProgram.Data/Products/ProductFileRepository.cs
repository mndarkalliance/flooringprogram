﻿using System;
using System.IO;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.Products
{
    public class ProductFileRepository : IProductRepository
    {
        // return product with designated product type from products.txt
        public Product ProductReader(string productType)
        {
            try
            {
                string[] data = File.ReadAllLines(@"Data\Products.txt");

                var product = new Product();

                for (int i = 1; i < data.Length; i++)
                {
                    string[] row = data[i].Split(',');

                    if (row[0].ToLower() == productType.ToLower())
                    {
                        product.ProductType = row[0];
                        product.MaterialCostPerSquareFoot = Convert.ToDecimal(row[1]);
                        product.LaborCostPerSquareFoot = Convert.ToDecimal(row[2]);
                        return product;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                ErrorLogger.LogException(ex);
                return null; 
            }

        }
    }
}
