﻿using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data.Products
{
    public class ProductRepositoryFactory
    {

            public static IProductRepository GetProductRepository()
            {
                switch (ConfigurationSettings.GetMode())
                {
                    case "Prod":
                        return new ProductFileRepository();
                    case "Test":
                        return new ProductTestRepository();
                }

                return null;
            }
        }

}

