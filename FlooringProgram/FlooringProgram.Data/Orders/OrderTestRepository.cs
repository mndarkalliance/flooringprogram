﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data
{
    public class OrderTestRepository : IOrderRepository
    {

        #region Test list of orders

        // create list of orders for test mode
        public static List<Order> TestOrders = new List<Order>()
            {
                new Order()
                {
                    OrderNumber = 1,
                    CustomerName = "Parker",
                    StateTaxRate = new TaxRate()
                    {
                        State = "OH",
                        TaxPercent = 0.10m
                    },
                    ProductInfo = new Product()
                    {
                        ProductType = "Wood",
                        MaterialCostPerSquareFoot = 5.1m,
                        LaborCostPerSquareFoot = 3.1m
                    },
                    Area = 1,
                    MaterialCost = 5.15m,
                    LaborCost = 4.75m,
                    Tax = 0.9900m,
                    Total = 10.8900m
                },
                 new Order()
                {
                    OrderNumber = 2,
                    CustomerName = "Murphy",
                    StateTaxRate = new TaxRate()
                    {
                        State = "OH",
                        TaxPercent = 0.10m
                    },
                    ProductInfo = new Product()
                    {
                        ProductType = "Wood",
                        MaterialCostPerSquareFoot = 5.1m,
                        LaborCostPerSquareFoot = 3.1m
                    },
                    Area = 1,
                    MaterialCost = 51.5m,
                    LaborCost = 47.5m,
                    Tax = 9.900m,
                    Total = 108.900m
                },
                  new Order()
                {
                    OrderNumber = 3,
                    CustomerName = "Merryweather",
                    StateTaxRate = new TaxRate()
                    {
                        State = "OH",
                        TaxPercent = 0.10m
                    },
                    ProductInfo = new Product()
                    {
                        ProductType = "Wood",
                        MaterialCostPerSquareFoot = 5.1m,
                        LaborCostPerSquareFoot = 3.1m
                    },
                    Area = 1,
                    MaterialCost = 51.5m,
                    LaborCost = 47.5m,
                    Tax = 9.900m,
                    Total = 108.900m
                }

        }; 
        #endregion


        // Read TestOrders (ignore filePath), set order to item with the specified orderNumber 
        // and return bool indicating whether order exists
        public Order OrderReader(string date, int orderNumber)  
        {
            Order order = new Order(); 

            // get a list of all orders that have the orderNumber specified in parameter
            var orders = TestOrders.Where(p => p.OrderNumber == orderNumber);

            // if the list has at least one item, assign that item to order
            if (orders.Any())
            {
                order = orders.First();   
                return order;              
            }                             
            
            return order;
        }

        // return list of test orders, ignore date
        public List<Order> GetOrders(string date)
        {
            return TestOrders;
        }

        // method is unnecessary in test mode, so return true and ignore parameters
        public bool Save(List<Order> orderList, string date)
        {
            return true; 
        }

        // test modes test file is always assigned to current date, so indicate
        // that file exists, only if current date is specified.
        public bool CheckForFile(string date)
        {  
            if (DateTime.Now.ToString("MMddyyyy") == date)
            {
                return true;
            }
            return false;
        }

        // Return what the next available index is  
        // in the order file from the specified date. 
        public int NextOrderNumber(string date)
        {
            return TestOrders.Count() + 1;
        }

        // file deletion is unnecessary in test mode so return true
        public bool DeleteFile(string date)
        {
            return true; 
        }
    }
}