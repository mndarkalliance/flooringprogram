﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;
using Newtonsoft.Json;

namespace FlooringProgram.Data.Orders
{
    public class OrderJsonRepository : IOrderRepository
    {

        private static string FilePath(string date)
        {
            return $@"Data\Orders_{date}.json";
        }

        // Read file at designated filePath, set order object corresponding to order number
        public Order OrderReader(string date, int orderNumber)
        {
            try
            {
                
                Order order = new Order(); 

                string jsonFile = File.ReadAllText(FilePath(date));
                List<Order> json = JsonConvert.DeserializeObject<List<Order>>(jsonFile);

                order = json.First(o => o.OrderNumber == orderNumber);
                
                return order;
                
            }
            catch (Exception ex)
            {
                ErrorLogger.LogException(ex);
                throw ex;

            }

        }

        // Return what the next available index is  
        // in the order file from the specified date. 
        public int NextOrderNumber(string date)
        {
            try
            {
                    string jsonFile = File.ReadAllText(FilePath(date));
                    List<Order> json = JsonConvert.DeserializeObject<List<Order>>(jsonFile);
                    return json.Count() + 1;
            }
            catch (Exception ex)
            {

                ErrorLogger.LogException(ex);
                throw ex;
            }

        }

        // Return a list of all orders in the order file from the specified date.
        public List<Order> GetOrders(string date)
        {
            List<Order> orderList = new List<Order>();

            try
            {
                string jsonFile = File.ReadAllText(FilePath(date));
                List<Order> json = JsonConvert.DeserializeObject<List<Order>>(jsonFile);

                return json;
                
            }
            catch (Exception ex)
            {
                ErrorLogger.LogException(ex);
                throw ex;
            }
        }

        // Save the specified list to the order file from the specified date.
        public bool Save(List<Order> orderList, string date)
        {
         
            try
            {
                string json = JsonConvert.SerializeObject(orderList.ToArray(), Formatting.Indented);
                File.WriteAllText(FilePath(date), json);
                
                return true;
            }
            catch (Exception ex)
            {
                ErrorLogger.LogException(ex);
                throw ex;
            }
        }

        // return bool indicating whether file exists from the specified date.
        public bool CheckForFile(string date)
        {
            return File.Exists(FilePath(date));
        }

        // delete file from the specified date
        public bool DeleteFile(string date)
        {
      
            try
            {
                File.Delete(FilePath(date));
                return true;
            }
            catch (Exception ex)
            {
                ErrorLogger.LogException(ex);
                throw ex;
            }

        }
    }
}
