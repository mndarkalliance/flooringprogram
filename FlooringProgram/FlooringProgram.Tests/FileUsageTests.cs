﻿using System;
using System.Configuration;
using FlooringProgram.Models;
using FlooringProgram.Operations;
using NUnit.Framework;

namespace FlooringProgram.Tests
{
    class FileUsageTests
    {

        [TestCase("Carpet", true)]
        [TestCase("Wood", true)]
        [TestCase("Human tears", false)]
        public static void GetProductTest(string productStr, bool expected)
        {
            Product product = ProductOperations.GetProduct(productStr);
                // if product exists it's returned
            
            Assert.AreEqual((product != null), expected);
        }

        [Test]
        public static void GetOrderNumber()
        {
            ConfigurationManager.AppSettings["mode"] = "Test";

            int orderNumber = OrderOperations.GetNextOrderNumber(DateTime.Now.ToString("MMddyyyy"));
                // 3 orders in Test List so next order number should be 4

            Assert.AreEqual(orderNumber, 4);
        }

    }
}
