﻿using FlooringProgram.Data.Products;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Operations
{
    public class ProductOperations
    {

        // returns Product with specified type
        public static Product GetProduct(string productType)
        {
            IProductRepository productRepo = ProductRepositoryFactory.GetProductRepository();

            productType = productType.Replace(" ", "");
            return productRepo.ProductReader(productType);
        }

 
    }
}
