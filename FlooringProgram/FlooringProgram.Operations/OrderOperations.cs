﻿using System.Collections.Generic;
using System.Linq;
using FlooringProgram.Data.Orders;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Operations
{

    public class OrderOperations
    {
        // retrieves order via order number from specified date
        public static Order GetOrder(string date, int orderNumber)
        {
            IOrderRepository orderRepo = OrderRepositoryFactory.GetOrderRepository();

            if (!orderRepo.CheckForFile(date) ||  // if file or order number does not exist
                !(GetNextOrderNumber(date) > orderNumber))          // return null
            {
                return null;
            }

            return orderRepo.OrderReader(date, orderNumber);
        }

        // Gets the next available order number for file from specified date
        public static int GetNextOrderNumber(string date)
        {
            IOrderRepository orderRepo = OrderRepositoryFactory.GetOrderRepository();
            return orderRepo.NextOrderNumber(date);
        }

        // saves specified order to file from specified date
        public static bool SaveOrder(Order order, string date)
        {
            IOrderRepository orderRepo = OrderRepositoryFactory.GetOrderRepository();
            List<Order> orders = new List<Order>();

            if (orderRepo.CheckForFile(date))
            {
                //get list of orders, add order to list, write list to file.

                orders = orderRepo.GetOrders(date);
                orders.Add(order);

                //return bool indicating whether save was successful
                return orderRepo.Save(orders, date);
            }
            else
            {
                orders.Add(order);
                return orderRepo.Save(orders, date);
            }

        }

        // saves specified edited order to file from specified date
        public static bool SaveEditedOrder(Order editedOrder, string date)
        {

            IOrderRepository orderRepo = OrderRepositoryFactory.GetOrderRepository();

            // get list of orders.
            List<Order> orders = orderRepo.GetOrders(date);

            // order orders by order number in an orderly fashion 
            orders = orders.OrderBy(s => s.OrderNumber).ToList();

            // replace order at correct index
            orders[editedOrder.OrderNumber - 1] = editedOrder;

            // save List to file and return bool indicating whether save was successful
            return orderRepo.Save(orders, date);

        }

        //used after an order is removed to a file so orders start at 1 and are sequencial 
        public static List<Order> EditOrderNumbers(List<Order> orders)
        {

            for (int i = 0; i < orders.Count; i++)
            {
                orders[i].OrderNumber = i + 1;
            }

            return orders;
        }

        //removes an order from a file. if it removes the last order from the file,
        //it removes the file as well
        public static bool RemoveOrder(string date, int orderNumber)
        {
            IOrderRepository orderRepo = OrderRepositoryFactory.GetOrderRepository();

            bool fileSaved = true;

            List<Order> orders = new List<Order>();

            Order order = GetOrder(date, orderNumber);


            if (order != null)
            {
                orders = orderRepo.GetOrders(date);

                orders.RemoveAt(order.OrderNumber - 1);

                orders = EditOrderNumbers(orders);

                if (orders.Count < 1) // delete file if there are no more orders in the list
                {
                    orderRepo.DeleteFile(date);
                }
                else // else save the edited list to file
                {
                    fileSaved = orderRepo.Save(orders, date); 
                }

                return fileSaved; // return true indicating success
            }

            return false; // return false indicating failure
        }

        // check if file for specified date exists
        public static bool CheckForFile(string date)
        {
            IOrderRepository orderRepo = OrderRepositoryFactory.GetOrderRepository();

            return orderRepo.CheckForFile(date);
        }

    }
}
