﻿using FlooringProgram.Models;

namespace FlooringProgram.Operations
{
    public class TotalsCalc
    {
        // These methods should be self-documenting.

        public static decimal TaxCalculator(Order order)
        {
            return order.StateTaxRate.TaxPercent * (order.LaborCost + order.MaterialCost);
        }

        public static decimal LaborCostCalculator(Order order)
        {
            return (decimal)order.Area * order.ProductInfo.LaborCostPerSquareFoot;
        }
 
        public static decimal MaterialCostCalculator(Order order)
        {
            return order.ProductInfo.MaterialCostPerSquareFoot * (decimal)order.Area; 
        }

        public static decimal TotalCalculatorCalculator(Order order)
        {
            return order.MaterialCost + order.LaborCost + order.Tax;
        }
    }

   

}
